# laravel-helper

## 项目简介

`laravel-helper`是一个laravel项目的助手包。

## 安装

1. `composer require top-songshijie/laravel-helper`
2. `config/app.php`文件中的`providers`数组中添加`SSJ\LaravelHelper\LhServiceProvider::class`
3. `laravel-helper/config/laravel-helper.php`复制到`config`文件夹下
4. `laravel-helper/database`目录下的sql执行一下

## 需要的`composer`包（按需安装，没有放在`composer.json`里面强制安装）

* qiniu/php-sdk:'7.*'
* aliyuncs/oss-sdk-php:'2.*'
* overtrue/easy-sms:'2.*'

