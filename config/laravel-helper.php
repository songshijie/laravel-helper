<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/14 11:20
 */

return [
    /**
     * 即时消息类型
     *
     * 企业微信：work
     * 飞书：feishu
     */
    'im_type'     => 'work',

    /**
     * 存储类型
     *
     * 本地存储：local
     * 阿里云存储：alioss
     * 七牛云存储：qiniu
     */
    'upload_type' => 'alioss',

    /**
     * 安全相关配置
     */
    'safe'        => [
        //签名key
        'sign_key' => '123456'
    ],

    /**
     * 清除文件
     *
     * message：提示语
     * dir：清除操作相对应的文件夹（相对于storage_path()函数）
     * exist_day：留存最近几天的文件
     */
    'clear_file'  => [
        ['message' => 'router日志', 'dir' => 'logs/router/', 'exist_day' => '7'],
        ['message' => 'info日志', 'dir' => 'logs/info/', 'exist_day' => '7'],
        ['message' => 'debug日志', 'dir' => 'logs/debug/', 'exist_day' => '7']
    ],

    /**
     * response相关配置
     */
    'response'    => [
        //返回的数据是否携带code码，如果携带的话默认携带的同http码
        'with_code'      => false,
        //返回的http码，default为跟随原参数，否则就是设置的值，比如设置200，那返回的http码就都是200
        'with_http_code' => 'default',
    ],

    /**
     * 日志相关配置
     */
    'log'         => [
        //router日志需要排除数据字段的路由，相对于laravel的"$request->path()"
        'router_except_data' => []
    ],

    /**
     * 短信相关配置
     */
    'sms'         => [
        //当debug为true时，模拟发送短信，验证码固定为1234
        'debug' => false
    ]
];
