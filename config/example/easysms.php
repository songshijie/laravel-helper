<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/23 17:50
 */

return [
    // HTTP 请求的超时时间（秒）
    'timeout'  => 10.0,

    // 默认发送配置
    'default'  => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

        // 默认可用的发送网关
        'gateways' => [
            'aliyun',
        ],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => storage_path("logs/easy-sms/" . sprintf('%s.log', date('Y-m'))),
        ],
        'aliyun'   => [
            'access_key_id'     => '',
            'access_key_secret' => '',
            'sign_name'         => '',
            'template_code'     => [
                'verify_code' => ''
            ]
        ],
    ],
];
