<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/20 12:06
 */

return [
    'domain'     => '',
    'bucket'     => '',
    'access_key' => '',
    'secret_key' => '',
];
