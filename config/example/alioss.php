<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/13 18:36
 */

return [
    'bucket'            => '',
    'endpoint'          => '',
    'access_key_id'     => '',
    'access_key_secret' => '',
];
