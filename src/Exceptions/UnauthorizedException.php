<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class UnauthorizedException extends BaseException
{
    public function __construct($message = '没有提供正确的认证信息')
    {
        parent::__construct($message, Response::HTTP_UNAUTHORIZED);
    }

    public function render()
    {
        return R::unauthorized($this->message);
    }
}
