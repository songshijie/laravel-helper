<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class MethodNotAllowedException extends BaseException
{
    public function __construct($message = '请求方法不被允许')
    {
        parent::__construct($message, Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function render()
    {
        return R::methodNotAllowed($this->message);
    }
}
