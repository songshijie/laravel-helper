<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class InternalServerErrorException extends BaseException
{
    protected $msgForUser;

    public function __construct($message, $msgForUser = '服务器错误')
    {
        parent::__construct($message, Response::HTTP_INTERNAL_SERVER_ERROR);

        $this->msgForUser = $msgForUser;
    }

    public function render()
    {
        return R::internalServerError($this->msgForUser);
    }
}
