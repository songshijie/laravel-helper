<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class NotFoundException extends BaseException
{
    public function __construct($message = '资源不存在')
    {
        parent::__construct($message, Response::HTTP_NOT_FOUND);
    }

    public function render()
    {
        return R::notFound($this->message);
    }
}
