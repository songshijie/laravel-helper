<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class UnprocessableEntityException extends BaseException
{
    public function __construct($message = '客户端请求参数验证不通过')
    {
        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function render()
    {
        return R::unprocessableEntity($this->message);
    }
}
