<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:43
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class BadRequestException extends BaseException
{
    public function __construct($message = '客户端请求格式错误')
    {
        parent::__construct($message, Response::HTTP_BAD_REQUEST);
    }

    public function render()
    {
        return R::badRequest($this->message);
    }
}