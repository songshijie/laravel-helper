<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/04/06 11:34
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class TooManyRequests extends BaseException
{
    public function __construct($message = '触发访问频率限制')
    {
        parent::__construct($message, Response::HTTP_TOO_MANY_REQUESTS);
    }

    public function render()
    {
        return R::tooManyRequests($this->message);
    }
}
