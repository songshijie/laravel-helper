<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/24 18:20
 */

namespace SSJ\LaravelHelper\Exceptions;

use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\R;

class Forbidden extends BaseException
{
    public function __construct($message = '请求的资源不允许访问')
    {
        parent::__construct($message, Response::HTTP_FORBIDDEN);
    }

    public function render()
    {
        return R::forbidden($this->message);
    }
}
