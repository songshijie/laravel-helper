<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 11:32
 */

use Illuminate\Support\Str;
use SSJ\LaravelHelper\Support\Im;
use SSJ\LaravelHelper\Support\Log;

if (!function_exists('lh_test')) {
    function lh_test()
    {
        return lh_laravel_version() . lh_php_version();
    }
}

if (!function_exists('lh_php_version')) {
    /**
     * 获取当前php版本
     *
     * @return string
     */
    function lh_php_version()
    {
        return PHP_VERSION;
    }
}

if (!function_exists('lh_laravel_version')) {
    /**
     * 获取当前laravel版本
     *
     * @return string
     */
    function lh_laravel_version()
    {
        return app()->version();
    }
}

if (!function_exists('lh_send_im_message')) {
    /**
     * 发送im通知
     *
     * @param string       $title
     * @param string|array $content
     * @param bool         $at
     * @param bool         $asyn
     *
     * @return mixed
     */
    function lh_send_im_message($title, $content, $at = false, $asyn = false)
    {
        if (config('laravel-helper.im_type') == 'work') {
            return Im::work($title, $content, $at, $asyn);
        }

        return Im::feishu($title, $content, $at, $asyn);
    }
}

if (!function_exists('lh_isset_not_empty')) {
    /**
     * 判断参数是否存在且不为空
     *
     * @param $param
     * @param $value
     *
     * @return false|mixed
     */
    function lh_isset_not_empty($param, $value)
    {
        if (!isset($param[$value])) {
            return false;
        }

        if (!empty($param[$value])) {
            return $param[$value];
        }

        if ($param[$value] === '0') {
            return '0';
        }

        if ($param[$value] === 0) {
            return 0;
        }

        return false;
    }
}

if (!function_exists('lh_page')) {
    /**
     * 通用分页
     *
     * @param      $builder
     * @param int  $pageNum
     * @param null $callback
     *
     * @return array
     */
    function lh_page($builder, $pageNum = 15, $callback = null)
    {
        $current_page = request()->input('page', 1);
        $total_num    = $builder->count();
        $total_page   = ceil($total_num / $pageNum);
        $list         = $builder->forPage($current_page, $pageNum)->get();

        is_callable($callback) && $callback($list);

        return [
            'current_page' => $current_page,
            'page_num'     => $pageNum,
            'total_num'    => $total_num,
            'total_page'   => $total_page,
            'list'         => $list
        ];
    }
}

if (!function_exists('lh_zh_json_encode')) {
    /**
     * 数据json化成，中文格式
     *
     * @param $data
     *
     * @return false|string
     */
    function lh_zh_json_encode($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}

if (!function_exists('lh_make_usable_no')) {
    /**
     * 生成一个可用的订单流水号
     *
     * @param $prefix
     * @param $random_length
     *
     * @return string
     * @throws Exception
     */
    function lh_make_usable_no($prefix, $random_length = 4)
    {
        $n = '';
        for ($i = 1; $i <= $random_length; $i++) {
            $n .= '9';
        }

        return $prefix . date('YmdHis') . str_pad(random_int(1, $n), $random_length, '0', STR_PAD_LEFT);
    }
}

if (!function_exists('lh_make_file_name')) {
    /**
     * 生成文件名
     *
     * @param $prefix
     * @param $suffix
     *
     * @return string
     */
    function lh_make_file_name($prefix, $suffix)
    {
        return $prefix . '_' . date('His') . '_' . Str::random(10) . '.' . $suffix;
    }
}

if (!function_exists('lh_fancy_time')) {
    /**
     * 获取花式的时间显示格式
     *
     * @param $timestamp
     *
     * @return array
     */
    function lh_fancy_time($timestamp)
    {
        $t = $timestamp && !empty($timestamp);

        return [
            $t ? $timestamp : 0,
            $t ? date('Y-m-d H:i:s', $timestamp) : '',
            $t ? date('Y-m-d H:i', $timestamp) : '',
        ];
    }
}

if (!function_exists('lh_info')) {
    /**
     * 记录info日志
     *
     * @param string       $title
     * @param string|array $content
     */
    function lh_info($title, $content)
    {
        Log::info(['标题' => $title, '内容' => $content]);
    }
}

if (!function_exists('lh_debug')) {
    /**
     * 记录debug日志
     *
     * @param string       $title
     * @param string|array $content
     */
    function lh_debug($title, $content)
    {
        if (config('app.debug')) {
            Log::debug(['标题' => $title, '内容' => $content]);
        }
    }
}

if (!function_exists('lh_is_mobile')) {
    /**
     * 验证是否是一个手机号
     *
     * @param $mobile
     *
     * @return boolean
     */
    function lh_is_mobile($mobile)
    {
        if (preg_match("/^1[3456789]\d{9}$/", $mobile)) {
            return true;
        }

        return false;
    }
}