<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:36
 */

namespace SSJ\LaravelHelper\Services\Upload;

class Factory
{
    protected $flag;

    public function __construct($flag)
    {
        $this->flag = $flag;
    }

    public function strategy($param)
    {
        $class = '\\SSJ\\LaravelHelper\\Services\\Upload\\Strategy\\' . ucfirst($this->flag) . 'Strategy';

        return new $class($param);
    }

}
