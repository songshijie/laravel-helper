<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Services\Im;


abstract class Strategy
{
    protected $app;

    abstract public function handle($param);

}
