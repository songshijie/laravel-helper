<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Services\Im;

class Factory
{
    protected $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function strategy()
    {
        $classes = [0 => 'feishu', 1 => 'work'];

        $class = '\\SSJ\\LaravelHelper\\Services\\Im\\Strategy\\' . ucfirst($classes[$this->type]) . 'Strategy';

        return new $class();
    }

}
