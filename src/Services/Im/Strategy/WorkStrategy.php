<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Services\Im\Strategy;

use GuzzleHttp\Client;
use SSJ\LaravelHelper\Services\Im\Strategy;

class WorkStrategy extends Strategy
{
    protected $client;

    public function __construct()
    {

        $this->client = new Client();
    }

    public function handle($param)
    {
        /* 所需参数 */
        $title        = $param['title'];
        $content      = (array)$param['content'];
        $at           = $param['at'];
        $at_mobile    = config('wechat.work.im.at_mobile');
        $web_hook_key = config('wechat.work.im.web_hook_key');

        /* 构造发送内容 */
        $message = '【标题】：' . $title . PHP_EOL . '【内容】：';
        $content = array_merge(['发送时间' => date('Y-m-d H:i:s'), '环境' => config('app.env')], $content);
        foreach ($content as $k => $v) {
            $message .= PHP_EOL . $k . '：' . $v;
        }

        /* 发送通知 */
        $this->client->request('POST',
            'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=' . $web_hook_key,
            [
                'json' => [
                    'msgtype' => 'text',
                    'text'    => [
                        'content'               => $message,
                        'mentioned_mobile_list' => $at ? [$at_mobile] : []
                    ]
                ]
            ]
        );
    }
}
