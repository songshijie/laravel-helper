<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Services\Im\Strategy;

use GuzzleHttp\Client;
use SSJ\LaravelHelper\Services\Im\Strategy;

class FeishuStrategy extends Strategy
{
    protected $client;

    public function __construct()
    {

        $this->client = new Client();
    }

    public function handle($param)
    {
        /* 所需参数 */
        $title        = $param['title'];
        $content      = (array)$param['content'];
        $at           = $param['at'];
        $at_user_id   = config('feishu.im.at_user_id');
        $web_hook_key = config('feishu.im.web_hook_key');

        /* 构造发送内容 */
        $message = '【标题】：' . $title . PHP_EOL . '【内容】：';
        $content = array_merge(['发送时间' => date('Y-m-d H:i:s'), '环境' => config('app.env')], $content);
        foreach ($content as $k => $v) {
            $message .= PHP_EOL . $k . '：' . $v;
        }

        if ($at) {
            $message .= "\n<at user_id = \"$at_user_id\"></at>";
        }

        /* 发送通知 */
        $this->client->request('POST',
            'https://open.feishu.cn/open-apis/bot/v2/hook/' . $web_hook_key,
            [
                'json' => [
                    'msg_type' => 'text',
                    'content'  => ['text' => $message]
                ]
            ]
        );
    }
}
