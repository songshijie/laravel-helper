<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Services\Im;

class Context
{
    private $strategy;

    public function __construct($strategy)
    {
        $this->strategy = $strategy;
    }

    public function handle($param)
    {
        return $this->strategy->handle($param);
    }

}
