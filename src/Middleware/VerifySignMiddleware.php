<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2023/03/24 18:35
 */

namespace SSJ\LaravelHelper\Middleware;

use Closure;
use Illuminate\Http\Response;
use SSJ\LaravelHelper\Support\Safe;
use SSJ\LaravelHelper\Exceptions\Forbidden;

class VerifySignMiddleware
{
    /**
     * 总处理
     *
     * @throws Forbidden
     */
    public function handle($request, Closure $next)
    {
        $param = $request->all();
        $sign  = $request->header('sign');

        Safe::verifySign($param, $sign);

        return $next($request);
    }
}
