<?php
/*
 * Author: top-songshijie
 * Email :997031758@qq.com
 * DateTime: 2023/06/14 09:42
 */

namespace SSJ\LaravelHelper\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Request as AppRequest;

class InitMiddleware
{
    /**
     * 总处理
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!AppRequest::hasMacro('uid')) {
            AppRequest::macro('uid', function () {
                return 0;
            });
        }

        if (!AppRequest::hasMacro('adminuid')) {
            AppRequest::macro('adminuid', function () {
                return 0;
            });
        }

        return $next($request);

    }
}
