<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/06/14 09:48
 */

namespace SSJ\LaravelHelper;

class Request extends \Illuminate\Http\Request
{
    /**
     * 获取请求的数据
     *
     * @param $params
     *
     * @return array
     */
    public function p(...$params)
    {
        $p = [];
        foreach ($params as $param) {
            if (!is_array($param)) {
                $p[$param] = is_null($this->input($param)) ? null : is_array($this->input($param)) ? $this->filterWord($this->input($param)) : $this->filterWord(trim($this->input($param)));
            } else {
                $p[$param[0]] = is_null($this->input($param[0], $param[1])) ? null : $this->filterWord(trim($this->input($param[0], $param[1])));
            }
        }
        unset($params);
        return $p;
    }

    /**
     * 过滤接收的参数
     *
     * @param string|array $str
     *
     * @return array|string|string[]|null
     */
    public function filterWord($str)
    {
        if (!$str) return $str;
        $farr = [
            "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
            "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
            "/select|join|where|drop|like|modify|rename|insert|update|table|database|alter|truncate|\'|\/\*|\.\.\/|\.\/|union|into|load_file|outfile/is"
        ];
        if (!is_array($str)) {
            $str = preg_replace($farr, '', $str);
        } else {
            foreach ($str as &$v) {
                if (is_array($v)) {
                    foreach ($v as &$vv) {
                        if (!is_array($vv)) $vv = preg_replace($farr, '', $vv);
                    }
                } else {
                    $v = preg_replace($farr, '', $v);
                }
            }
        }
        return $str;
    }
}
