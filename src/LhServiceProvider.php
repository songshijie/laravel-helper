<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/13 15:19
 */

namespace SSJ\LaravelHelper;

use Illuminate\Support\ServiceProvider;
use SSJ\LaravelHelper\Console\PublishCommand;
use SSJ\LaravelHelper\Middleware\InitMiddleware;
use SSJ\LaravelHelper\Console\Schedule\ClearFile;
use SSJ\LaravelHelper\Middleware\VerifySignMiddleware;
use SSJ\LaravelHelper\Console\Schedule\BackupDatabase;
use SSJ\LaravelHelper\Middleware\PreventShakeMiddleware;
use SSJ\LaravelHelper\Middleware\MakeRequestTraceIdMiddleware;


class LhServiceProvider extends ServiceProvider
{
    const VERSION = '2.11.2';

    protected $commands = [
        PublishCommand::class,
        ClearFile::class,
        BackupDatabase::class
    ];

    protected $routeMiddleware = [
        'lh.verify_sign'           => VerifySignMiddleware::class,
        'lh.prevent_shake'         => PreventShakeMiddleware::class,
        'lh.make_request_trace_id' => MakeRequestTraceIdMiddleware::class,
        'lh.init'                  => InitMiddleware::class,
    ];

    public function register()
    {
        $this->commands($this->commands);

        $this->registerRouteMiddleware();
    }

    public function boot()
    {
        $path = realpath(__DIR__ . '/../config/laravel-helper.php');
        $this->publishes([$path => config_path('laravel-helper.php')], 'config');
        $this->mergeConfigFrom($path, 'laravel-helper');
    }

    protected function registerRouteMiddleware()
    {
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }
    }
}
