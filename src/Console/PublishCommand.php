<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 17:15
 */

namespace SSJ\LaravelHelper\Console;

use Illuminate\Console\Command;

class PublishCommand extends Command
{
    protected $signature = 'lh:publish';

    protected $description = "发布 laravel-helper 功能包";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        echo 'This is PublishCommand';
    }
}
