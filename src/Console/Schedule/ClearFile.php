<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/04/07 10:51
 */

namespace SSJ\LaravelHelper\Console\Schedule;

use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearFile extends Command
{
    protected $signature = 'lh:schedule:clear-file';

    protected $description = '删除系统生成的临时文件及过期的日志文件';

    public function handle()
    {
        $msg = [];

        $clear_files = config('laravel-helper.clear_file');

        foreach ($clear_files as $clear_file) {
            if (!lh_isset_not_empty($clear_file, 'message')) {
                continue;
            }
            if (!lh_isset_not_empty($clear_file, 'dir')) {
                continue;
            }
            if (!lh_isset_not_empty($clear_file, 'exist_day')) {
                continue;
            }

            try {
                $dir = Carbon::now()->subDays($clear_file['exist_day'])->toDateString();

                $path = storage_path($clear_file['dir']) . $dir;
                system('rm -rf ' . $path);
            } catch (\Throwable $throwable) {
                $msg[$clear_file['message']] = $throwable->getMessage();
            }

            $msg[$clear_file['message']] = $dir;
        }

        lh_info($this->description, $msg);
    }
}
