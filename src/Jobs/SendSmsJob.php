<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/23 17:42
 */

namespace SSJ\LaravelHelper\Jobs;

use Overtrue\EasySms\EasySms;
use Illuminate\Bus\Queueable;
use SSJ\LaravelHelper\Support\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Overtrue\EasySms\Exceptions\Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $param;

    /**
     * SendSmsJob constructor.
     *
     * @param $param ['mobile','verify_code']
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * 发送短信
     */
    public function handle()
    {
        $mobile      = $this->param['mobile'];
        $verify_code = $this->param['verify_code'];

        if (!config('laravel-helper.sms.debug')) {
            try {
                $app = new EasySms(config('easysms'));
                $app->send($mobile, [
                    'template' => config('easysms.gateways.aliyun.template_code.verify_code'),
                    'data'     => [
                        'code' => $verify_code
                    ],
                ]);
            } catch (Exception $exception) {
                Log::exception($exception, $exception->getExceptions());
                return false;

            } catch (\Throwable $throwable) {
                Log::exception($throwable);
                return false;
            }
        }

        lh_info('短信发送成功', [
            'mobile'      => $mobile,
            'verify_code' => $verify_code
        ]);

        return true;
    }
}
