<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/13 18:14
 */

namespace SSJ\LaravelHelper\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use SSJ\LaravelHelper\Services\Im\Context;
use SSJ\LaravelHelper\Services\Im\Factory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $param;

    /**
     * ImJob constructor.
     *
     * @param $param ['type'] 剩下的参数参考各个策略
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * 发送im通知
     */
    public function handle()
    {
        $factory = new Factory($this->param['type']);

        $context = new Context($factory->strategy());

        $context->handle($this->param);
    }
}
