<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 17:15
 */

namespace SSJ\LaravelHelper\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * 附件模型
 *
 * @property int    id
 * @property int    type                  类型
 * @property string name                  文件原名
 * @property string mime                  文件mine
 * @property string ext                   文件扩展名
 * @property string size                  文件大小
 * @property string md5                   md5加密
 * @property string sha1                  sha1加密
 * @property string upload_ip             上传客户端ip
 * @property string bucket                阿里云bucket
 * @property string path                  路径
 * @property string preview_path          预览路径
 * @property string alioss_preview_path   阿里oss预览路径
 * @property string local_preview_path    本地预览路径
 */
class Attachment extends BaseModel
{
    protected $table = 'lh_attachment';

    const TYPE_0 = 0;
    const TYPE_1 = 1;

    public static $typeMap = [
        self::TYPE_0 => '图片',
        self::TYPE_1 => '文件',
    ];

    /**
     * 检测文件是否存在
     *
     * @param $md5
     * @param $sha1
     *
     * @return Builder|Model|object|null
     */
    public static function check($md5,$sha1)
    {
        return self::query()->where('md5', $md5)->where('sha1', $sha1)->first();
    }
}
