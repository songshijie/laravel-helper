<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:36
 */

namespace SSJ\LaravelHelper\Support;

use SSJ\LaravelHelper\Models\Attachment;
use SSJ\LaravelHelper\Services\Upload\Context;
use SSJ\LaravelHelper\Services\Upload\Factory;

class Upload
{
    /**
     * 上传图片
     *
     * @param       $file
     * @param int   $max_size
     * @param array $allowed_ext
     *
     * @return mixed
     */
    public static function image($file, $max_size = 0, $allowed_ext = [])
    {
        $param = [
            'file'     => $file, 'attachment_type' => Attachment::TYPE_0,
            'max_size' => $max_size, 'allowed_ext' => $allowed_ext
        ];

        $strategy = (new Factory(config('laravel-helper.upload_type')))->strategy($param);

        return (new Context($strategy))->handle();
    }

    /**
     * 上传文件
     *
     * @param       $file
     * @param int   $max_size
     * @param array $allowed_ext
     *
     * @return mixed
     */
    public static function file($file, $max_size = 0, $allowed_ext = [])
    {
        $param = [
            'file'     => $file, 'attachment_type' => Attachment::TYPE_1,
            'max_size' => $max_size, 'allowed_ext' => $allowed_ext
        ];

        $strategy = (new Factory(config('laravel-helper.upload_type')))->strategy($param);

        return (new Context($strategy))->handle();
    }
}
