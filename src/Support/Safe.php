<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/24 18:30
 */

namespace SSJ\LaravelHelper\Support;

use SSJ\LaravelHelper\Exceptions\Forbidden;
use SSJ\LaravelHelper\Services\Safe\SignService;

class Safe
{
    /**
     * 生成签名
     *
     * @param $param
     *
     * @return string
     */
    public static function makeSign($param)
    {
        $signService = new SignService();

        return $signService->makeSign($param);
    }

    /**
     * 验证签名
     *
     * @param $param
     * @param $sign
     *
     * @return bool
     * @throws Forbidden
     */
    public static function verifySign($param, $sign)
    {
        $signService = new SignService();

        $bool = $signService->verifySign($param, $sign);

        if (!$bool) {
            throw new Forbidden('签名验证失败');
        }

        return $bool;
    }
}
