<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:41
 */

namespace SSJ\LaravelHelper\Support;

use Illuminate\Support\Str;
use SSJ\LaravelHelper\Jobs\ImJob;

class Im
{
    /**
     * 飞书
     *
     * @param string       $title
     * @param string|array $content
     * @param boolean      $at
     * @param boolean      $asyn
     *
     * @return mixed
     */
    public static function feishu($title, $content, $at, $asyn)
    {
        $param = ['type' => 0, 'title' => $title, 'content' => $content, 'at' => $at];

        $laravel_version = lh_laravel_version();

        /* laravel版本小于6的情况 */
        if ($laravel_version < '6') {
            return ImJob::dispatch($param);
        }

        /* laravel版本等于6的情况 */
        if(Str::startsWith($laravel_version,'6'))
        {
            return ImJob::dispatchNow($param);
        }

        /* 其他情况 */
        if ($asyn) {
            return ImJob::dispatch($param);
        } else {
            return ImJob::dispatchSync($param);
        }
    }

    /**
     * 企业微信
     *
     * @param string       $title
     * @param string|array $content
     * @param boolean      $at
     * @param boolean      $asyn
     *
     * @return mixed
     */
    public static function work($title, $content, $at, $asyn)
    {
        $param = ['type' => 1, 'title' => $title, 'content' => $content, 'at' => $at];

        /* laravel版本小于6的情况 */
        if (lh_laravel_version() < '6') {
            return ImJob::dispatch($param);
        }

        /* 其他情况 */
        if ($asyn) {
            return ImJob::dispatch($param);
        } else {
            return ImJob::dispatchSync($param);
        }
    }
}
