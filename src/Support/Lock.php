<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/06/14 17:45
 */

namespace SSJ\LaravelHelper\Support;

use Illuminate\Support\Facades\Redis;
use SSJ\LaravelHelper\Services\Lock\LockService;

class Lock
{
    /**
     * @param string   $key 锁的key值
     * @param callable $fn  回调函数
     * @param int      $ex  过期时间，防止死锁
     *
     * @return mixed
     */
    public static function exec($key, $fn, int $ex = 6)
    {
        $lockService = new LockService();

        return $lockService->exec($key, $fn, $ex);
    }
}
