<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/02/22 14:36
 */

namespace SSJ\LaravelHelper\Support;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class R
{
    /**
     * 一般性成功
     *
     * @param string|array|null $data
     * @param string            $msg
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function ok($data = null, $msg = 'success', $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_OK, $code);
    }

    /**
     * 客户端请求参数验证不通过
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function unprocessableEntity($msg = 'HTTP_UNPROCESSABLE_ENTITY', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_UNPROCESSABLE_ENTITY, $code);
    }

    /**
     * 服务器错误
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function internalServerError($msg = 'HTTP_INTERNAL_SERVER_ERROR', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_INTERNAL_SERVER_ERROR, $code);
    }

    /**
     * 资源不存在
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function notFound($msg = 'HTTP_NOT_FOUND', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_NOT_FOUND, $code);
    }

    /**
     * 客户端请求格式错误
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function badRequest($msg = 'HTTP_BAD_REQUEST', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_BAD_REQUEST, $code);
    }

    /**
     * 没有提供正确的认证信息
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function unauthorized($msg = 'HTTP_UNAUTHORIZED', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_UNAUTHORIZED, $code);
    }

    /**
     * 请求方法不被允许
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function methodNotAllowed($msg = 'HTTP_METHOD_NOT_ALLOWED', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_METHOD_NOT_ALLOWED, $code);
    }

    /**
     * 请求的资源不允许访问（没有权限）
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function forbidden($msg = 'HTTP_FORBIDDEN', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_FORBIDDEN, $code);
    }

    /**
     * 触发访问频率限制
     *
     * @param string            $msg
     * @param string|array|null $data
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function tooManyRequests($msg = 'HTTP_TOO_MANY_REQUESTS', $data = null, $code = null)
    {
        return self::json([
            'message' => $msg,
            'data'    => $data
        ], Response::HTTP_TOO_MANY_REQUESTS, $code);
    }

    /**
     * json形式原样返回
     *
     * @param string|array|null $data
     * @param int               $status
     * @param int|null          $code
     *
     * @return JsonResponse
     */
    public static function json($data, $status, $code = null)
    {
        if (!is_null($code)) {
            $data = array_merge($data, ['code' => $code]);
        } elseif (config('laravel-helper.response.with_code')) {
            $data = array_merge($data, ['code' => $status]);
        }

        if (config('laravel-helper.response.with_http_code') !== 'default') {
            $status = config('laravel-helper.response.with_http_code');
        }

        $data = array_merge($data, ['time' => date('Y-m-d H:i:s')]);

        Log::router($data);

        return response()->json($data, $status);
    }
}
