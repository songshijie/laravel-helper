<?php
/*
 * Author  : top-songshijie
 * Email   : 997031758@qq.com
 * DateTime: 2023/03/23 09:41
 */

namespace SSJ\LaravelHelper\Support;

use App\Jobs\SendSmsJob;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\InvalidRequestException;
use SSJ\LaravelHelper\Services\Sms\SmsService;
use SSJ\LaravelHelper\Exceptions\UnprocessableEntityException;

class Sms
{
    /**
     * 发送短信验证码
     *
     * @param string $mobile
     *
     * @return array
     * @throws UnprocessableEntityException
     */
    public static function send($mobile, $asyn = false)
    {
        $smsService = new SmsService();

        return $smsService->send($mobile, $asyn);
    }

    /**
     * 验证验证码是否正确
     *
     * @param string $mobile
     * @param string $verify_key
     * @param string $verify_code
     * @param bool   $is_destroy
     *
     * @return bool
     * @throws UnprocessableEntityException
     */
    public static function verify($mobile, $verify_key, $verify_code, $is_destroy = true)
    {
        $smsService = new SmsService();

        return $smsService->verify($mobile, $verify_key, $verify_code, $is_destroy);
    }
}
